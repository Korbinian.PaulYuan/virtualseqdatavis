﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class createCubesPos : MonoBehaviour {

	public int numberElements;
	public string filename;

	List<CoordsCube> cubelist = new List<CoordsCube> ();

	public List<GameObject> tumorSeqCubes;

	// Use this for initialization
	void Start () {
		//putCubetoScene (numberElements, 30, 5);
		putCubesfromcsv(filename);
		//TextAsset dats = Resources.Load<TextAsset> (filename);
		//string[] data = dats.text.Split (new char[] { '\n' });
		//Debug.Log (data.Length);


	}
	

	private void putCubetoScene(int num_el, float deg, float radius) {
	
		for (int i = 0; i < num_el; i++) {
			//GameObject plane = GameObject.CreatePrimitive (PrimitiveType.Plane);
			GameObject plane = GameObject.CreatePrimitive (PrimitiveType.Cube);
			const float PI = 3.1415926f;
			float alpha = i * deg;
			plane.transform.position = new Vector3 (-radius * Mathf.Sin (PI*alpha/180), 0, radius * Mathf.Cos (PI*alpha/180));
			plane.transform.rotation = Quaternion.Euler (0, -alpha, 0);

			tumorSeqCubes.Add(plane);
		}
	}

	//load cube from csv file
	private void putCubesfromcsv(string filename){
		string[] test = File.ReadAllLines(filename);
		Debug.Log (test.Length);
		for (int i = 1; i < test.Length; i++) {
			string[] row = test [i].Split (new char[] { ',' });
			CoordsCube q = new CoordsCube ();
			q.id = i-1;
			float.TryParse (row[0],out q.x);
			float.TryParse (row[1],out q.y); 
			float.TryParse (row[2],out q.z); 
			float.TryParse (row[3],out q.clridx); 

			cubelist.Add (q);
		}

		foreach (CoordsCube q in cubelist) {
			//Debug.Log (q.x);
			GameObject cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
			cube.transform.position = new Vector3 (q.x, q.y, q.z);
			float size = 0.5f;
			cube.transform.localScale = new Vector3 (size, size, size);
			Renderer rend = cube.GetComponent<Renderer>();
            //rend.material.SetColor("_Color", Color.green);
            rend.material.color = getColorFromIdx(q.clridx);// new Color (1.0f,q.clridx,q.clridx,q.clridx);
			tumorSeqCubes.Add(cube);
		}
			
	}

    private Color getColorFromIdx(float clridx)
    {
        Color clr = new Color(clridx, 1.0f - clridx, Mathf.Max(1.0f - 2*clridx,0.0f),  clridx / 2 + 0.45f);
        return clr;

    }
}
